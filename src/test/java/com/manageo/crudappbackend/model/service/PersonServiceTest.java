package com.manageo.crudappbackend.model.service;

import com.manageo.crudappbackend.exceptions.DuplicateResourceException;
import com.manageo.crudappbackend.exceptions.ResourceNotFoundException;
import com.manageo.crudappbackend.model.dto.PersonDTO;
import com.manageo.crudappbackend.model.entities.PersonEntity;
import com.manageo.crudappbackend.model.mappers.PersonMapper;
import com.manageo.crudappbackend.model.repositories.PersonRepository;
import com.manageo.crudappbackend.model.services.PersonService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
public class PersonServiceTest {
    @Mock
    private PersonRepository personRepository;

    @InjectMocks
    private PersonService personService;

    @Test
    void testFindByIdExistingPerson() {
        UUID id = UUID.randomUUID();
        PersonEntity personEntity = new PersonEntity();
        personEntity.setId(id);
        when(personRepository.findById(id)).thenReturn(Optional.of(personEntity));

        Optional<PersonDTO> result = personService.findById(id);

        assertTrue(result.isPresent());
        assertEquals(id, result.get().getId());
    }

    @Test
    void testFindByIdNonExistingPerson() {
        UUID id = UUID.randomUUID();
        when(personRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> personService.findById(id));
    }

    @Test
    void testCreateNewPerson() {
        PersonDTO personDTO = new PersonDTO();
        personDTO.setFirstName("Alice");
        personDTO.setLastName("Smith");
        personDTO.setEmail("alice@example.com");

        when(personRepository.findByEmail(personDTO.getEmail())).thenReturn(Optional.empty());
        when(personRepository.save(any())).thenReturn(PersonMapper.INSTANCE.toEntity(personDTO));

        PersonDTO result = personService.create(personDTO);

        assertNotNull(result);
        assertEquals("Alice", result.getFirstName());
        assertEquals("Smith", result.getLastName());
        assertEquals("alice@example.com", result.getEmail());
    }

    @Test
    void testCreateDuplicatePerson() {
        PersonDTO personDTO = new PersonDTO();
        personDTO.setEmail("alice@example.com");
        when(personRepository.findByEmail(personDTO.getEmail())).thenReturn(Optional.of(new PersonEntity()));

        assertThrows(DuplicateResourceException.class, () -> personService.create(personDTO));
    }

    @Test
    void testUpdateExistingPerson() {
        UUID id = UUID.randomUUID();
        PersonDTO personDTO = new PersonDTO();
        personDTO.setFirstName("Updated");
        personDTO.setLastName("Person");
        personDTO.setEmail("updated@example.com");

        PersonEntity existingEntity = new PersonEntity();
        existingEntity.setId(id);
        existingEntity.setFirstName("Original");
        existingEntity.setLastName("Person");
        existingEntity.setEmail("original@example.com");

        when(personRepository.findById(id)).thenReturn(Optional.of(existingEntity));
        when(personRepository.findByEmail(personDTO.getEmail())).thenReturn(Optional.empty());
        when(personRepository.save(any(PersonEntity.class))).thenReturn(existingEntity);

        Optional<PersonDTO> result = personService.update(id, personDTO);

        assertNotNull(result.get());
        assertEquals("Updated", result.get().getFirstName());
        assertEquals("Person", result.get().getLastName());
        assertEquals("updated@example.com", result.get().getEmail());
    }

    @Test
    void testUpdateNonExistingPerson() {
        UUID id = UUID.randomUUID();
        PersonDTO personDTO = new PersonDTO();
        personDTO.setEmail("new@example.com");

        when(personRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> personService.update(id, personDTO));
    }

    @Test
    void testUpdateWithDuplicateEmail() {
        UUID id = UUID.randomUUID();
        PersonDTO personDTO = new PersonDTO();
        personDTO.setEmail("duplicate@example.com");

        PersonEntity existingEntity = new PersonEntity();
        existingEntity.setId(UUID.randomUUID());
        existingEntity.setEmail("duplicate@example.com");

        when(personRepository.findById(id)).thenReturn(Optional.of(existingEntity));
        when(personRepository.findByEmail(personDTO.getEmail())).thenReturn(Optional.of(existingEntity));

        assertThrows(DuplicateResourceException.class, () -> personService.update(id, personDTO));
    }

    @Test
    void testDeleteExistingPerson() {
        UUID id = UUID.randomUUID();
        when(personRepository.findById(id)).thenReturn(Optional.of(new PersonEntity()));

        personService.delete(id);

        verify(personRepository, times(1)).deleteById(id);
    }

    @Test
    void testDeleteNonExistingPerson() {
        UUID id = UUID.randomUUID();
        when(personRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> personService.delete(id));
    }
}