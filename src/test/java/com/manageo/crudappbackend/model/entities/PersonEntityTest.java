package com.manageo.crudappbackend.model.entities;


import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class PersonEntityTest {

    @Test
    public void testGettersAndSetters() {
        PersonEntity person = new PersonEntity();
        UUID id = UUID.randomUUID();

        person.setId(id);
        person.setFirstName("John");
        person.setLastName("Doe");
        person.setEmail("john.doe@example.com");

        assertEquals(person.getId(), id);
        assertEquals(person.getFirstName(), "John");
        assertEquals(person.getLastName(), "Doe");
        assertEquals(person.getEmail(), "john.doe@example.com");
    }
}
