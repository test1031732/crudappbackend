package com.manageo.crudappbackend.model.dto;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class PersonDTOTest {

    private final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    private final Validator validator = factory.getValidator();

    @Test
    public void testValidPersonDTO() {
        PersonDTO personDTO = new PersonDTO();
        personDTO.setId(UUID.randomUUID());
        personDTO.setFirstName("John");
        personDTO.setLastName("Doe");
        personDTO.setEmail("john.doe@example.com");

        Set<ConstraintViolation<PersonDTO>> violations = validator.validate(personDTO);

        assertEquals(0, violations.size());
    }

    @Test
    public void testInvalidPersonDTO() {
        PersonDTO personDTO = new PersonDTO();
        personDTO.setId(null);
        personDTO.setFirstName("");
        personDTO.setLastName("");
        personDTO.setEmail("invalid.email");

        Set<ConstraintViolation<PersonDTO>> violations = validator.validate(personDTO);

        assertEquals(4, violations.size());
    }
}
