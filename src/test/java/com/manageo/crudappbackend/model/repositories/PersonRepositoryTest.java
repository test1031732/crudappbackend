package com.manageo.crudappbackend.model.repositories;

import com.manageo.crudappbackend.model.entities.PersonEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class PersonRepositoryTest {

    @Autowired
    private PersonRepository personRepository;

    @Test
    public void testFindAll() {
        List<PersonEntity> allPersons = personRepository.findAll();
        assertEquals(10, allPersons.size());
    }

    @Test
    public void testFindByEmailExistingEmail() {
        Optional<PersonEntity> personEntity = personRepository.findByEmail("john.doe@example.com");

        assertTrue(personEntity.isPresent());
        assertEquals("John", personEntity.get().getFirstName());
        assertEquals("Doe", personEntity.get().getLastName());
    }

    @Test
    public void testFindByEmailNonExistingEmail() {
        Optional<PersonEntity> foundPersonOptional = personRepository.findByEmail("nonexisting@example.com");
        assertFalse(foundPersonOptional.isPresent());
    }


    @Test
    public void testFindByIdExistingId() {
        Optional<PersonEntity> foundPersonOptional = personRepository.findById(UUID.fromString("736d8e1d-61d7-4f46-8c73-1c1c02f4e01a"));

        assertTrue(foundPersonOptional.isPresent());
        assertEquals("John", foundPersonOptional.get().getFirstName());
        assertEquals("Doe", foundPersonOptional.get().getLastName());
    }

    @Test
    public void testFindByIdNonExistingId() {
        Optional<PersonEntity> foundPersonOptional = personRepository.findById(UUID.randomUUID());

        assertFalse(foundPersonOptional.isPresent());
    }

    @Test
    public void testDeleteByIdExistingId() {
        UUID id = UUID.randomUUID();
        PersonEntity person = new PersonEntity();
        person.setId(id);
        person.setFirstName("David");
        person.setLastName("Miller");
        person.setEmail("david@example.com");
        personRepository.save(person);

        personRepository.deleteById(id);

        Optional<PersonEntity> deletedPersonOptional = personRepository.findById(id);
        assertFalse(deletedPersonOptional.isPresent());
    }
}