package com.manageo.crudappbackend.rest.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.manageo.crudappbackend.model.dto.PersonDTO;
import com.manageo.crudappbackend.model.services.PersonService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class PersonControllerTest {

    @Mock
    private PersonService personService;

    @InjectMocks
    private PersonController personController;

    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(personController).build();
        objectMapper = new ObjectMapper();
    }

    @Test
    void testFindAll() throws Exception {
        PersonDTO person1 = new PersonDTO();
        PersonDTO person2 = new PersonDTO();
        when(personService.findAll()).thenReturn(Arrays.asList(person1, person2));

        mockMvc.perform(get("/api/persons"))
                .andExpect(status().isOk());
    }

    @Test
    void testFindByIdExisting() throws Exception {
        UUID id = UUID.randomUUID();
        PersonDTO person = new PersonDTO();
        when(personService.findById(id)).thenReturn(Optional.of(person));

        mockMvc.perform(get("/api/persons/{id}", id))
                .andExpect(status().isOk());
    }

    @Test
    void testFindByIdNonExisting() throws Exception {
        UUID id = UUID.randomUUID();
        when(personService.findById(id)).thenReturn(Optional.empty());

        mockMvc.perform(get("/api/persons/{id}", id))
                .andExpect(status().isNotFound());
    }

    @Test
    void testCreate() throws Exception {
        PersonDTO personDTO = new PersonDTO();
        personDTO.setFirstName("Alice");
        personDTO.setLastName("Smith");
        personDTO.setEmail("alice@example.com");

        when(personService.create(any())).thenReturn(personDTO);

        mockMvc.perform(post("/api/persons")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(personDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("Alice"))
                .andExpect(jsonPath("$.lastName").value("Smith"))
                .andExpect(jsonPath("$.email").value("alice@example.com"));
    }

    @Test
    void testUpdateExisting() throws Exception {
        UUID id = UUID.randomUUID();
        PersonDTO personDTO = new PersonDTO();
        personDTO.setFirstName("Updated");
        personDTO.setLastName("Person");
        personDTO.setEmail("updated@example.com");

        when(personService.update(eq(id), any())).thenReturn(Optional.of(personDTO));

        mockMvc.perform(put("/api/persons/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(personDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("Updated"))
                .andExpect(jsonPath("$.lastName").value("Person"))
                .andExpect(jsonPath("$.email").value("updated@example.com"));
    }

    @Test
    void testDelete() throws Exception {
        UUID id = UUID.randomUUID();
        mockMvc.perform(delete("/api/persons/{id}", id))
                .andExpect(status().isNoContent());
    }
}