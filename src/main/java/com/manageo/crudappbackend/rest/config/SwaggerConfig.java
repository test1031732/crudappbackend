package com.manageo.crudappbackend.rest.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(
                contact = @Contact(
                        name = "CrudApp Backend",
                        email = "example@gmail.com",
                        url = "https://crudapp.com"
                ),
                description = "API Documentation for CrudApp Backend",
                title = "CrudApp API",
                version = "1.0",
                license = @License(
                        name = "License name",
                        url = "https://crudapp.com/license"
                ),
                termsOfService = "Terms of service"
        )
)
@Configuration
public class SwaggerConfig {
}