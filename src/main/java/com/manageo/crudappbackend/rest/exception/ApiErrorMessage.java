package com.manageo.crudappbackend.rest.exception;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.UUID;

@Setter
@Getter
@Builder
@ToString
public class ApiErrorMessage {
    @Builder.Default
    private UUID id = UUID.randomUUID();
    private String message;
    @Builder.Default
    private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    @Builder.Default
    private String localDateTime = LocalDateTime.now().toString();
    private String relatedObject;
}