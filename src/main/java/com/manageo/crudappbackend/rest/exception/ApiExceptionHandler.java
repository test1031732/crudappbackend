package com.manageo.crudappbackend.rest.exception;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import com.manageo.crudappbackend.exceptions.DuplicateResourceException;
import com.manageo.crudappbackend.exceptions.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@ControllerAdvice
public class ApiExceptionHandler {
    private final ObjectMapper objectMapper;

    public ApiExceptionHandler() {
        this.objectMapper = createObjectMapper();
    }

    private ObjectMapper createObjectMapper() {
        // Create and configure the custom ObjectMapper
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        JavaTimeModule javaTimeModule = new JavaTimeModule();
        javaTimeModule.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer(DateTimeFormatter.ISO_DATE_TIME));
        javaTimeModule.addSerializer(LocalDate.class, new LocalDateSerializer(DateTimeFormatter.ISO_DATE));
        javaTimeModule.addSerializer(LocalTime.class, new LocalTimeSerializer(DateTimeFormatter.ISO_TIME));
        objectMapper.registerModule(javaTimeModule);
        return objectMapper;
    }

    @ExceptionHandler(DuplicateResourceException.class)
    public ResponseEntity<ApiErrorMessage> handleConflictException(DuplicateResourceException ex) throws JsonProcessingException {
        String json = objectMapper.writeValueAsString(ex.getObj());

        ApiErrorMessage message = ApiErrorMessage.builder()
                .message(ex.getMessage())
                .httpStatus(ex.getStatus())
                .relatedObject(json)
                .build();


        return new ResponseEntity<>(message, message.getHttpStatus());
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ApiErrorMessage> handleResourceNotFoundException(ResourceNotFoundException ex) throws JsonProcessingException {
        String json = objectMapper.writeValueAsString(ex.getObj());

        ApiErrorMessage message = ApiErrorMessage.builder()
                .message(ex.getMessage())
                .httpStatus(ex.getStatus())
                .relatedObject(json)
                .build();


        return new ResponseEntity<>(message, message.getHttpStatus());
    }
}