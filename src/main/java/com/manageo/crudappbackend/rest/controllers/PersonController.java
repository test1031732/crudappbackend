package com.manageo.crudappbackend.rest.controllers;

import com.manageo.crudappbackend.model.dto.PersonDTO;
import com.manageo.crudappbackend.model.services.PersonService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Tag(name = "Applications", description = "Applications API Endpoint")
@RestController
@RequestMapping("/api/persons")
public class PersonController {

    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @Operation(
            description = "Get Endpoint for Persons",
            summary = "Get Endpoint for Persons",
            responses = {
                    @ApiResponse(description = "Success", responseCode = "200"),
            }
    )
    @GetMapping
    public ResponseEntity<List<PersonDTO>> findAll() {
        List<PersonDTO> persons = personService.findAll();
        return ResponseEntity.ok(persons);
    }

    @Operation(
            description = "Get Endpoint for Persons By Id",
            summary = "Get Endpoint for Persons By Id",
            responses = {
                    @ApiResponse(description = "Success", responseCode = "200"),
                    @ApiResponse(description = "Resource Not Found", responseCode = "404")
            }
    )
    @GetMapping("/{id}")
    public ResponseEntity<PersonDTO> findById(@PathVariable UUID id) {
        return personService.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());

    }

    @Operation(
            description = "Create Persons Endpoint",
            summary = "Create Persons Endpoint",
            responses = {
                    @ApiResponse(description = "Created", responseCode = "201"),
                    @ApiResponse(description = "Resource Not Found", responseCode = "404"),
                    @ApiResponse(description = "Conflict", responseCode = "409")
            }
    )
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public ResponseEntity<PersonDTO> create(@Valid @RequestBody PersonDTO personDTO) {
        PersonDTO createdPerson = personService.create(personDTO);
        return ResponseEntity.ok(createdPerson);
    }

    @Operation(
            description = "Update Persons Endpoint",
            summary = "Update Persons Endpoint",
            responses = {
                    @ApiResponse(description = "Success", responseCode = "200"),
                    @ApiResponse(description = "Resource Not Found", responseCode = "404"),
            }
    )
    @PutMapping("/{id}")
    public ResponseEntity<PersonDTO> update(@PathVariable UUID id, @RequestBody PersonDTO personDTO) {
        return personService.update(id, personDTO)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @Operation(
            description = "Delete Persons Endpoint",
            summary = "Delete Persons Endpoint",
            responses = {
                    @ApiResponse(description = "Success", responseCode = "200"),
                    @ApiResponse(description = "Resource Not Found", responseCode = "404"),
            }
    )
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable UUID id) {
        personService.delete(id);
        return ResponseEntity.noContent().build();
    }
}