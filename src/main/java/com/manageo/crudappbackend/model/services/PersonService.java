package com.manageo.crudappbackend.model.services;

import com.manageo.crudappbackend.exceptions.DuplicateResourceException;
import com.manageo.crudappbackend.exceptions.ResourceNotFoundException;
import com.manageo.crudappbackend.model.dto.PersonDTO;
import com.manageo.crudappbackend.model.entities.PersonEntity;
import com.manageo.crudappbackend.model.mappers.PersonMapper;
import com.manageo.crudappbackend.model.repositories.PersonRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;


@Transactional
@Service
public class PersonService {

    private final PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public Optional<PersonDTO> findById(UUID id) {
        Optional<PersonEntity> byId = personRepository.findById(id);
        if (byId.isPresent()) {
            return byId.map(PersonMapper.INSTANCE::toDTO);
        } else {
            throw ResourceNotFoundException.builder()
                    .message("Person not found [" + id + "]")
                    .obj(id)
                    .build();
        }
    }

    public List<PersonDTO> findAll() {
        List<PersonEntity> allPerson = personRepository.findAll();
        return allPerson.stream()
                .map(PersonMapper.INSTANCE::toDTO)
                .collect(Collectors.toList());
    }

    public PersonDTO create(PersonDTO personDTO) {
        PersonEntity byEmail = personRepository.findByEmail(personDTO.getEmail()).orElse(null);
        if (byEmail != null) {
            throw DuplicateResourceException.builder()
                    .message("Person with email [" + personDTO.getEmail() + "] already exists")
                    .obj(personDTO)
                    .build();
        }
        PersonEntity entityToSave = PersonMapper.INSTANCE.toEntity(personDTO);
        PersonEntity savedEntity = personRepository.save(entityToSave);
        return PersonMapper.INSTANCE.toDTO(savedEntity);
    }

    public Optional<PersonDTO> update(UUID id, PersonDTO personDTO) {
        //Verifier que la personne a mettre a jours existe deja
        PersonEntity entityToUpdate = personRepository.findById(id)
                .orElseThrow(() -> ResourceNotFoundException.builder()
                        .message("Person not found [" + id + "]")
                        .obj(personDTO)
                        .build());

        //Verifier si une autre personne (avec ID different) a le meme email que personDTO
        PersonEntity byEmail = personRepository.findByEmail(personDTO.getEmail()).orElse(null);
        if (byEmail != null && !byEmail.getId().equals(id)) {
            throw DuplicateResourceException.builder()
                    .message("Another Person with email [" + personDTO.getEmail() + "] already exists")
                    .obj(personDTO)
                    .build();
        }

        entityToUpdate.setEmail(personDTO.getEmail());
        entityToUpdate.setFirstName(personDTO.getFirstName());
        entityToUpdate.setLastName(personDTO.getLastName());

        PersonEntity savedEntity = personRepository.save(entityToUpdate);
        return Optional.of(PersonMapper.INSTANCE.toDTO(savedEntity));
    }

    public void delete(UUID id) {
        personRepository.findById(id)
                .orElseThrow(() -> ResourceNotFoundException.builder()
                        .message("Person not found [" + id + "]")
                        .obj(id)
                        .build());
        personRepository.deleteById(id);
    }
}