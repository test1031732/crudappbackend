package com.manageo.crudappbackend.exceptions;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Setter
@Getter
@Builder
public class ResourceNotFoundException extends RuntimeException {

    private final String message;
    @Builder.Default
    private final HttpStatus status = HttpStatus.NOT_FOUND;
    private final Object obj;
}