package com.manageo.crudappbackend.exceptions;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Setter
@Getter
@Builder
public class DuplicateResourceException extends RuntimeException {

    private final String message;
    @Builder.Default
    private final HttpStatus status = HttpStatus.CONFLICT;
    private final Object obj;
}