CREATE TABLE person
(
    id         UUID         NOT NULL,
    first_name VARCHAR(255) NOT NULL,
    last_name  VARCHAR(255) NOT NULL,
    email      VARCHAR(255) NOT NULL,
    CONSTRAINT pk_person PRIMARY KEY (id)
);

ALTER TABLE person
    ADD CONSTRAINT uc_person_email UNIQUE (email);