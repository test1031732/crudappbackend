INSERT INTO person (id, first_name, last_name, email)
VALUES ('736d8e1d-61d7-4f46-8c73-1c1c02f4e01a', 'John', 'Doe', 'john.doe@example.com'),
       ('52f21ea7-0a37-4b47-9ae0-962581550c14', 'Jane', 'Smith', 'jane.smith@example.com'),
       ('13e2e6df-8d7b-4a2e-9355-3e695ee25e99', 'Michael', 'Johnson', 'michael.johnson@example.com'),
       ('6d53b8e5-b8f2-4c3f-8a6e-67a5e9ee8be6', 'Emily', 'Williams', 'emily.williams@example.com'),
       ('41c7a3b2-4500-4b21-a315-79d1bceaa82d', 'Daniel', 'Brown', 'daniel.brown@example.com'),
       ('87c40f2b-d5ed-42d0-a8b5-c8a021344bee', 'Olivia', 'Jones', 'olivia.jones@example.com'),
       ('a6431f16-3a84-456c-a2c2-6c0f2f9e0c6f', 'William', 'Davis', 'william.davis@example.com'),
       ('ef45e605-d6c3-4d04-bec2-8fffeaae1d29', 'Sophia', 'Miller', 'sophia.miller@example.com'),
       ('49b7c994-94a0-4b6c-b66d-d4b4da27d7b0', 'James', 'Wilson', 'james.wilson@example.com'),
       ('2d7e456f-6ac4-4e0f-a9a0-c451370bd09f', 'Ava', 'Taylor', 'ava.taylor@example.com');