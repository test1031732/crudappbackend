#!/bin/bash
if [[ $# -eq 0 ]] 
then 
	echo "Building project with Maven"
	mvn clean install
	if [[ $? -eq 0 ]] 
	then
	    echo "Successfully built project"
	    echo "Running Spring Boot application"
	    java -jar target/*.jar
	else
	    echo "Build failed. Exiting script."
	    exit 1
	fi
elif [[ $1 -eq 'run' ]] 
then 
	    echo "Run Without Build"
	    echo "Running Spring Boot application"
	    java -jar target/*.jar
fi