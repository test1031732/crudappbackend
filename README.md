## Dependencies

| Dependency                          | Description                                                                         | 
|-------------------------------------|-------------------------------------------------------------------------------------|
| h2                                  | base de données relationnelle en mémoire                                            |
| mapstruct                           | MapStruct est pour simplifier la conversion entre les différents modèles de données |
| springdoc-openapi-starter-webmvc-ui | génère automatiquement une documentation OpenAPI pour les API REST et la validation |
| hibernate-validator (scope Test)    | pour pouvoir tester les validation dans le DTO                                      |
| mapstruct-processor                 | pour pouvoir tester le mapper (pour generer le impl dans les test)                  |

## Swagger

Pour la documentation swagger de l'API, utilisez les URL

- http://localhost:8080/crudappbackend/api-docs
- http://localhost:8080/crudappbackend/swagger-ui/index.html

## Contenu de l'Application

L'application est structurée en plusieurs classes, chacune jouant un rôle spécifique dans la gestion des données des
personnes.

### PersonEntity

Cette classe représente l'entité persistante dans la base de données qui stocke les informations sur une personne.

### PersonDTO

Cette classe représente l'objet de transfert de données (DTO) pour les informations sur une personne. Elle est utilisée
pour les opérations de création, mise à jour et affichage des données.
Fournit une couche d'abstraction pour transférer les données entre les couches de l'application.
Les annotations telles que @NotBlank, @Email et @Pattern sont utilisées pour valider les données entrées.

### PersonMapper

Cette interface est générée automatiquement par MapStruct et elle est utilisée pour la conversion entre les entités
PersonEntity et les DTO PersonDTO.

### PersonRepository

Cette interface étend JpaRepository et gère l'interaction avec la base de données pour les entités PersonEntity.

### PersonService

Cette classe gère la logique métier liée aux opérations sur les personnes.
